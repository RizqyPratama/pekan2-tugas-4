<?php

class animal
{
    public $legs = 2;
    public $cold_blooded = false;

    public function __construct($name, $legs, $cold_blooded)
    {
        $this->name = $name;
        $this->legs = $legs;
        $this->cold_blooded = $cold_blooded;
    }

    public function set_name($name1)
    {
        $this->name = $name1;
    }

    public function get_name()
    {
        return $this->name;
    }

    public function set_legs($legs1)
    {
        $this->legs = $legs1;
    }

    public function get_legs()
    {
        return $this->legs;
    }

    public function set_cold_blooded($cb)
    {
        $this->cold_blooded = $cb;
    }

    public function get_cold_blooded()
    {
        return $this->cold_blooded;
    }
}
