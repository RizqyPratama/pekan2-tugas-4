<?php

require_once "animal.php";
require_once "frog.php";
require_once "apee.php";

$animal1 = new animal("shaun", 2, false);

echo "<h1>Release 0</h1>";

echo $animal1->get_name() . "<br>";

echo $animal1->get_legs() . " Legs <br>";
echo $animal1->get_cold_blooded();

echo "<h1>Release 1</h1>";

$sungokong = new apee("Kera Sakti", 4, false);


echo $sungokong->get_name() . "<br>";
echo $sungokong->get_yell();


$frog = new frog("Buduk", 4, false);


echo "<br><br>";

echo $frog->get_name() . "<br>";
echo $frog->get_gerak();
